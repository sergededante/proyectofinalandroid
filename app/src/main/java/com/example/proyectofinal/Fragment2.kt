package com.example.proyectofinal

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.jetbrains.anko.doAsync
import org.json.JSONObject


class Fragment2 : Fragment() {

    private lateinit var recyclerView: RecyclerView

    companion object{
        fun newInstance():Fragment2{
            return  Fragment2()
        }
    }
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_2, container, false)
        recyclerView = rootView.findViewById(R.id.rvEspaciosLinea) as RecyclerView
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = layoutManager

        getEspaciosJson(activity!!)
    }

    private fun getEspaciosJson(context: Context){
        doAsync {
            val requestQueue = Volley.newRequestQueue(activity!!)

            var strRequest = object : StringRequest(Method.POST,"http://tester.cucea.udg.mx/espacios/espaciosJson.php", Response.Listener {
                    s-> Log.e("TAG", s.toString())

                val res = JSONObject(s)
                var espaciosJson= listOf<Espacios>()
                for (i in 0 until res.getJSONArray("espacios").length()) {
                    espaciosJson += Espacios(
                        res.getJSONArray("espacios").getJSONObject(i).getString("evento"),
                        res.getJSONArray("espacios").getJSONObject(i).getString("nombre"),
                        res.getJSONArray("espacios").getJSONObject(i).getString("lugar"),
                        res.getJSONArray("espacios").getJSONObject(i).getString("cupo"),
                        res.getJSONArray("espacios").getJSONObject(i).getString("disponibilidad")
                    )
                }
                val adaptador = AdaptadorEspacios(context, espaciosJson)
                recyclerView.adapter = adaptador
            },Response.ErrorListener {
                    e-> Log.e("TAG", e.toString())
            }){}

            requestQueue.add(strRequest)
        }
    }

}
