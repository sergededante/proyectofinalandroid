package com.example.proyectofinal

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

class Fragment1 : Fragment() {

    private lateinit var recyclerView: RecyclerView

    companion object {
        fun newInstance(): Fragment1 {
            return Fragment1()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.fragment_1, container, false)

        recyclerView = rootView.findViewById(R.id.rvEspacios) as RecyclerView

        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        recyclerView.layoutManager = layoutManager

        setEspacios(activity!!)
    }

    private fun setEspacios(context: Context){
        var Espacios : List<Espacios> = listOf(
            Espacios("Google en el Mundo","Sala 1","Auditorio Central", "150", "25"),
            Espacios("Intel en CUCEA","Aula Amplia 2","Nucle de Aulas", "120", "2"),
            Espacios("Software Libre","Auditorio CERI","CERI", "70", "Lleno"),
            Espacios("Taller de Android","Laboratorio L102","Módulo L", "24", "10"),
            Espacios("Google en el Mundo","Sala 1","Auditorio Central", "150", "25"),
            Espacios("Conferencia Phyton","Aula Amplia 2","Nucle de Aulas", "120", "Lleno"),
            Espacios("Hacking Ético","Auditorio CERI","CERI", "70", "Lleno"),
            Espacios("Taller de Kotlin","Laboratorio L102","Módulo L", "24", "10")
        )
        val adaptador = AdaptadorEspacios(context,Espacios)
        recyclerView.adapter = adaptador
    }
}