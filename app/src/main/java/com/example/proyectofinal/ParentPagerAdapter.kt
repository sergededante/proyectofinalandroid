package com.example.mesaayuda

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.example.proyectofinal.Fragment1
import com.example.proyectofinal.Fragment2
import com.example.proyectofinal.Fragment3

class ParentPagerAdapter (manager : FragmentManager) : FragmentPagerAdapter(manager){
    override fun getItem(posicion : Int): Fragment {
        return when(posicion){
            0 -> Fragment1.newInstance()
            1 -> Fragment2.newInstance()
            else-> Fragment3.newInstance()
        }
    }

    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "Eventos"
            1 -> "Eventos En linea"
            else-> "Web"
        }
    }
}