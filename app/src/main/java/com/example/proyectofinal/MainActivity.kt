package com.example.proyectofinal

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    private lateinit var etUser: EditText
    private lateinit var etPass: EditText
    private lateinit var btnLogin: Button
    private lateinit var btnRegistro: Button
    private lateinit var user: String
    private lateinit var pass: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initComponents()
        setListeners()
    }


    private fun initComponents() {
        etUser = findViewById(R.id.etUser)
        etPass = findViewById(R.id.etPass)
        btnLogin = findViewById(R.id.btnLogin)
        btnRegistro = findViewById(R.id.btnRegistro)
    }

    private fun setListeners() {
        btnLogin.setOnClickListener {
            var prefs: SharedPreferences = getSharedPreferences("TableMTI", Context.MODE_PRIVATE)
            user = prefs.getString("user", "no existe")
            pass = prefs.getString("pass", "no existe")
            if (etUser.getText().toString() == user && etPass.text.toString() == pass) {
                //Mensaje PopUp
                Toast.makeText(this, "Bienvenido " + etUser.text.toString(), Toast.LENGTH_SHORT).show()
                val intent = Intent(this, HomeActivity::class.java)
                intent.putExtra("nombre", "Sergio De Dante")
                startActivity(intent)

                /*
                var editor:SharedPreferences.Editor = getSharedPreferences("TableMTI", Context.MODE_PRIVATE).edit()
                editor.putString("apellido", "Palomares")
                editor.commit()
                */
            } else {
                Toast.makeText(this, "Usuario y/o contraseña " + user, Toast.LENGTH_SHORT).show()
            }

        }

        btnRegistro.setOnClickListener {
            val intent = Intent(this, RegistroActivity::class.java)

            startActivity(intent)
        }

        /*txtEjemplo.setOnClickListener(

        )*/
    }

}