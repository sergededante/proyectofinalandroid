package com.example.proyectofinal

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import android.support.annotation.RequiresApi
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.os.Build
import android.widget.Toast

class Fragment3 : Fragment() {


    companion object {

        fun newInstance(): Fragment3 {
            return Fragment3()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView = inflater.inflate(R.layout.fragment_3, container, false)

        val mWebView = rootView.findViewById(R.id.webView) as WebView
        mWebView.loadUrl("https://www.google.com/")

        val webSettings = mWebView.settings
        webSettings.javaScriptEnabled = true

        mWebView.webViewClient = WebViewClient()


        return rootView
    }

}
