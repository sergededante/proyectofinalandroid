package com.example.proyectofinal


import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.cardview.view.*

class AdaptadorEspacios(val context: Context, var Espacios:List<Espacios>) : RecyclerView.Adapter<AdaptadorEspacios.EspaciosAdaptador>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): EspaciosAdaptador {
        val view = LayoutInflater.from(context).inflate(R.layout.cardview, p0, false)
        return EspaciosAdaptador(view)
    }

    override fun getItemCount(): Int {
        return Espacios.size
    }

    override fun onBindViewHolder(adapter: EspaciosAdaptador, pos: Int) {
        return adapter.setData(Espacios[pos], pos)
    }


    inner class EspaciosAdaptador(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun setData(espacio: Espacios, pos: Int) {
            itemView.txtEvento.text = "Evento: " + espacio.evento
            itemView.txtNombre.text = "Lugar: " + espacio.nombre
            itemView.txtLugar.text = "Cupo: " + espacio.diponibilidad

            if (espacio.diponibilidad == "Lleno") {
                itemView.setBackgroundColor(android.graphics.Color.parseColor("#444444"))

            }
        }
    }
}