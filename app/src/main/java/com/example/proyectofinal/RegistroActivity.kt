package com.example.proyectofinal

import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast

class RegistroActivity : AppCompatActivity() {

    private lateinit var  etUserReg : EditText
    private  lateinit var  etPassReg : EditText
    //private  lateinit var  etSexoReg : EditText
    //private  lateinit var  etEdadReg : EditText

    private lateinit var  btnRegReg : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registro)

        initComponents()
        setListeners()

    }


    private fun initComponents(){
        etUserReg = findViewById(R.id.etUserReg)
        etPassReg = findViewById(R.id.etPassReg)
        //etSexoReg = findViewById(R.id.etSexoReg)
        //etEdadReg = findViewById(R.id.etEdadReg)

        btnRegReg = findViewById(R.id.btnRegReg)
    }

    private fun setListeners() {
        btnRegReg.setOnClickListener {

            if (etUserReg.getText().toString() != "" && etPassReg.text.toString() != "") {


                var editor: SharedPreferences.Editor = getSharedPreferences("TableMTI", Context.MODE_PRIVATE).edit()
                editor.putString("user", etUserReg.getText().toString())
                editor.putString("pass", etPassReg.getText().toString())
                editor.commit()

                //Mensaje PopUp
                Toast.makeText(
                    this,
                    "El registro se realizó exitosamente " + etUserReg.text.toString(),
                    Toast.LENGTH_SHORT
                ).show()

                this.finish()

            } else {
                Toast.makeText(this, "Usuario y/o contraseña no deben estar vacios", Toast.LENGTH_SHORT).show()
            }

        }


    }
}
