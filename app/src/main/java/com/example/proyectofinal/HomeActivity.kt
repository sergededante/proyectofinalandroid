package com.example.proyectofinal

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.TabLayout
import android.support.v4.app.FragmentManager
import android.support.v4.view.ViewPager
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.example.mesaayuda.ParentPagerAdapter
import java.io.FileReader

class HomeActivity : AppCompatActivity() {

    private lateinit var tabs: TabLayout
    private lateinit var viewpPager: ViewPager
    //private lateinit var fabEjemplo: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        //setting toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        //home navigation
        supportActionBar?.setDisplayHomeAsUpEnabled(false)


        initComponents()



        val adapter = ParentPagerAdapter(supportFragmentManager)
        viewpPager.adapter = adapter
        tabs.setupWithViewPager(viewpPager, true)


        /*fabEjemplo.setOnClickListener {
            Toast.makeText(this, "Soy un boton flotante", Toast.LENGTH_SHORT).show()
        }*/

    }

    private fun initComponents(){
        tabs = findViewById(R.id.tabs)
        viewpPager = findViewById(R.id.viewPager)
        //fabEjemplo = findViewById(R.id.fabEjemplo)
    }


    //setting menu in action bar
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.custom_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    // actions on click menu items
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_logout -> {
            var editorDatos: SharedPreferences.Editor = getSharedPreferences("TableMTIDatos", Context.MODE_PRIVATE).edit()
            var editorReg: SharedPreferences.Editor = getSharedPreferences("TableMTI", Context.MODE_PRIVATE).edit()

            editorDatos.clear().commit()
            editorReg.clear().commit()

            Toast.makeText(this,"Logout",Toast.LENGTH_SHORT).show()
            this.finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            true
        }
        R.id.action_perfil -> {
            val intent = Intent(this, PerfilActivity::class.java)
            //intent.putExtra("nombre", "Sergio De Dante")
            startActivity(intent)
            //Toast.makeText(this,"Perfil",Toast.LENGTH_SHORT).show()
            true
        }
        /*android.R.id.home ->{
            Toast.makeText(this,"Home action",Toast.LENGTH_SHORT).show()
            true
        }*/

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }
}