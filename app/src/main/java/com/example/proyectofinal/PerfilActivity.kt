package com.example.proyectofinal

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Button
import android.widget.EditText
import android.widget.Toast


import android.app.AlertDialog
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.ImageView
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

class PerfilActivity : AppCompatActivity() {

    private lateinit var  etNomPer : EditText
    private  lateinit var  etApPer : EditText
    private  lateinit var  etGenPer : EditText
    private  lateinit var  etEdadPer : EditText

    private lateinit var  btnGP : Button
    private lateinit var  btnDP : Button
    private lateinit var  btnSI : Button


    private var btn: Button? = null
    private var imageview: ImageView? = null
    private val GALLERY = 1



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perfil)

        initComponents()
        setListeners()

        //setting toolbar
        setSupportActionBar(findViewById(R.id.toolbar))
        //home navigation
        supportActionBar?.setDisplayHomeAsUpEnabled(true)



        btn = findViewById<View>(R.id.btnSI) as Button
        imageview = findViewById<View>(R.id.ivUserPerfil) as ImageView

        btn!!.setOnClickListener { choosePhotoFromGallary() }

    }


    //setting menu in action bar
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.custom_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    // actions on click menu items
    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_logout -> {

            var editorDatos: SharedPreferences.Editor = getSharedPreferences("TableMTIDatos", Context.MODE_PRIVATE).edit()
            var editorReg: SharedPreferences.Editor = getSharedPreferences("TableMTI", Context.MODE_PRIVATE).edit()

            editorDatos.clear().commit()
            editorReg.clear().commit()

            Toast.makeText(this,"Logout",Toast.LENGTH_SHORT).show()

            this.finish()
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            true

        }
        /*R.id.action_perfil -> {
            val intent = Intent(this, PerfilActivity::class.java)
            //intent.putExtra("nombre", "Sergio De Dante")
            startActivity(intent)
            //Toast.makeText(this,"Perfil",Toast.LENGTH_SHORT).show()
            true
        }*/
        android.R.id.home ->{
            Toast.makeText(this," Regresar al Home",Toast.LENGTH_SHORT).show()
            this.finish()
            true

        }

        else -> {
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            super.onOptionsItemSelected(item)
        }
    }


    private fun initComponents(){
        etNomPer = findViewById(R.id.etNombre)
        etApPer = findViewById(R.id.etApellido)
        etEdadPer = findViewById(R.id.etEdadPerfil)
        etGenPer = findViewById(R.id.etGeneroPerfil)

        btnGP = findViewById(R.id.btnGuardarPerfil)
        btnDP = findViewById(R.id.btnDatoPerfil)

    }


    private fun setListeners() {
        btnGP.setOnClickListener {

            if (etNomPer.getText().toString() != "" && etApPer.text.toString() != "") {


                var editor: SharedPreferences.Editor = getSharedPreferences("TableMTIDatos", Context.MODE_PRIVATE).edit()
                editor.putString("nombre", etNomPer.getText().toString())
                editor.putString("apellido", etApPer.getText().toString())
                editor.putString("genero", etGenPer.getText().toString())
                editor.putString("edad", etEdadPer.getText().toString())
                editor.commit()

                //Mensaje PopUp
                Toast.makeText(
                    this,
                    "El registro se realizó exitosamente " + etNomPer.text.toString(),
                    Toast.LENGTH_SHORT
                ).show()

                //this.finish()

            } else {
                Toast.makeText(this, "Nombre y/o Apellido no deben estar vacios", Toast.LENGTH_SHORT).show()
            }

        }

        btnDP.setOnClickListener {



                var prefs: SharedPreferences = getSharedPreferences("TableMTIDatos", Context.MODE_PRIVATE)
                var nombre = prefs.getString("nombre", "no existe")
                var ap = prefs.getString("apellido", "no existe")
                var genero = prefs.getString("genero", "no existe")
                var edad = prefs.getString("edad", "no existe")

            if (nombre != "no existe" && ap != "no existeqwe    ") {

                //Mensaje PopUp
                Toast.makeText(
                    this,
                    "Tus datos: Nombre " + nombre.toString() + " Apellido " + ap.toString() + " Genero " + genero.toString() + " Edad " + edad.toString(),
                    Toast.LENGTH_LONG
                ).show()

                //this.finish()

            } else {
                Toast.makeText(this, "Datos no registrados", Toast.LENGTH_SHORT).show()
            }

        }



    }



    fun choosePhotoFromGallary() {
        val galleryIntent = Intent(Intent.ACTION_PICK,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI)

        startActivityForResult(galleryIntent, GALLERY)
    }


    public override fun onActivityResult(requestCode:Int, resultCode:Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)
        /* if (resultCode == this.RESULT_CANCELED)
         {
         return
         }*/
        if (requestCode == GALLERY)
        {
            if (data != null)
            {
                val contentURI = data!!.data
                try
                {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    val path = saveImage(bitmap)
                    Toast.makeText(this, "Imagen Guardada!" + path, Toast.LENGTH_SHORT).show()
                    imageview!!.setImageBitmap(bitmap)

                }
                catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this, "Failed!", Toast.LENGTH_SHORT).show()
                }

            }

        }
    }

    fun saveImage(myBitmap: Bitmap):String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + IMAGE_DIRECTORY)
        // have the object build the directory structure, if needed.
        Log.d("fee",wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists())
        {

            wallpaperDirectory.mkdirs()
        }

        try
        {
            Log.d("heel",wallpaperDirectory.toString())
            val f = File(wallpaperDirectory, ((Calendar.getInstance()
                .getTimeInMillis()).toString() + ".jpg"))
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(this,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null)
            fo.close()
            Log.d("TAG", "Archivo guardado::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        }
        catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }

    companion object {
        private val IMAGE_DIRECTORY = "/demonuts"
    }






}
